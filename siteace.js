Websites = new Mongo.Collection("websites");

//security 
Websites.allow({
	insert:function(userId, doc){
		console.log('adding website');
		if (Meteor.user()){
			if (userId != doc.createdBy){
				return false;
			}
			else {
				return true;
			}
		}
		else {
			return false;	
		}
	},
	update:function(){
		if (Meteor.user()){
		return true;
		}
		else {
		return false;
		}
	},
	remove:function(userId, doc){
		console.log("insert security");
		if (Meteor.user()){
			if (userId != doc.createdBy){
				return false;
			}
			else {
				return true;
			}
		}
		else {
			return false;
		}
	}
})

//Search engine implementation
	WebsitesIndex = new EasySearch.Index({
	collection: Websites,
	fields: ['title', 'description'],
	engine: new EasySearch.MongoDB({
		sort: function() {
			return {'rating':-1};
		}
	}),
	placeholder: "Search..."
	
});



if (Meteor.isClient) {

	Meteor.subscribe("websites");
	Meteor.subscribe("users");

	Accounts.ui.config({
		passwordSignupFields: "USERNAME_AND_EMAIL"
	});
	
	Comments.ui.config({
		template: 'bootstrap'
	});

	Router.configure({
		layoutTemplate: 'ApplicationLayout'
	});
	
	Router.route('/', function(){
		this.render('website_form', {
		to:'main1'
		});
		this.render('searchBox', {
		to:'main2',
		data:function(){
			return Websites.find({}, {sort:{rating:-1}});
			}
		});
	});
	
	Router.route('/:__originalId', function(){
		this.render('website_item', {
		to:'main1',
		data:function(){
  			return Websites.findOne({_id:this.params.__originalId});
  			}
		});
		this.render('myComments',{
		to: 'main2',
		});
	});

	 
    
	// helper function that returns all available websites
	
	Template.web_list.helpers({
		websites:function(){
			return Websites.find({}, {sort:{rating:-1}});
		}
	});
	
	Template.website_items.helpers({
		websites:function(){
			return Websites.find({}, {sort:{rating:-1}});
		},
		getUser:function(user_id){
    	var user = Meteor.users.findOne({_id:user_id});
    	if (user){
    		return user.username;
    	}
    	else {
    		return "anon";
    	}
    }
	});
	
	Template.searchBox.helpers({
  		websitesIndex: () => WebsitesIndex
	});

	Template.body.helpers({username:function(){
  		if (Meteor.user()){
  			return Meteor.user().username;
  			//return Meteor.user().emails[0].address;
  		}
  		else {
  			return "dear visitor";
  		}
  	  }
  	});
	
	/////
	//  template events 
	/////
	
	Template.website_items.events({
		"click .js-upvote":function(event){
			var website_id = this.__originalId;
			var rating = this.rating++;
			
			var upvotes = this.upvotes++;
			var upvotes = this.upvotes;
			var rating = this.rating++;
			Websites.update({_id:website_id}, 
                    {$set: {rating:rating++, upvotes:upvotes++}});
			
            //console.log(this.rating); 
            //console.log(this.upvotes);        
			return false;// prevent the button from reloading the page
		}, 
		"click .js-downvote":function(event){
			var website_id = this.__originalId;
			var rating = this.rating;
			
			var downvotes = this.downvotes++;
			var downvotes = this.downvotes;
			
			Websites.update({_id:website_id}, 
                    {$set: {rating:rating-1, downvotes:downvotes++}});

			//console.log(this.rating);
			//console.log(this.downvotes);
			return false;
		},
		"click .js-delete":function(event){
			var website_id = this.__originalId;
			console.log(website_id);
			$("#"+website_id).hide('slow', function(){
				Websites.remove({"_id":website_id});
			});
		}
	})

	Template.website_form.events({
		"click .js-toggle-website-form":function(event){
			$("#website_form").show('slow');
		}, 
		"submit .js-save-website-form":function(event){
			var url = event.target.url.value;
			var title = event.target.title.value;
			var description = event.target.description.value;
			
			if (Meteor.user()){
          		Websites.insert({
            	url:url, 
            	title:title,
            	description:description, 
            	createdOn:new Date(),
            	rating:0,
            	upvotes:0,
            	downvotes:0,
            	createdBy:Meteor.user()._id
          		});
          		$("#website_form").hide('slow', function(){;
          			alert("You Web-Site is added to the list");
          			});
          		}
				return false;// stop the form submit from reloading the page
			},
		"click .js-cancel-website-form":function(event){
			$("#website_form").hide('slow');
		}
	});		
}


if (Meteor.isServer) {
	// start up function that creates entries in the Websites databases.
	
  Meteor.publish("websites", function(){
  	return Websites.find();
  })  
  
   Meteor.publish("users", function(){
  	users = Meteor.users.find();
  	return users;
  })  
  	
  Meteor.startup(function () {
  	
    // code to run on server at startup
    if (!Websites.findOne()){
    	console.log("No websites yet. Creating starter data.");
    	  Websites.insert({
    		title:"Goldsmiths Computing Department", 
    		url:"http://www.gold.ac.uk/computing/", 
    		description:"This is where this course was developed.", 
    		createdOn:new Date(),
    		rating:0,
            upvotes:0,
            downvotes:0
    	});
    	 Websites.insert({
    		title:"University of London", 
    		url:"http://www.londoninternational.ac.uk/courses/undergraduate/goldsmiths/bsc-creative-computing-bsc-diploma-work-entry-route", 
    		description:"University of London International Programme.", 
    		createdOn:new Date(),
    		rating:0,
            upvotes:0,
            downvotes:0
    	});
    	 Websites.insert({
    		title:"Coursera", 
    		url:"http://www.coursera.org", 
    		description:"Universal access to the world’s best education.", 
    		createdOn:new Date(),
    		rating:0,
            upvotes:0,
            downvotes:0,
            comment:0
    	});
    	Websites.insert({
    		title:"Google", 
    		url:"http://www.google.com", 
    		description:"Popular search engine.", 
    		createdOn:new Date(),
    		rating:0,
            upvotes:0,
            downvotes:0
    	});
    }
    
  });
  
}
